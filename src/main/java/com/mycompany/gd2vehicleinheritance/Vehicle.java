/*
 * Challenge.
 * Start with a base class of a Vehicle, then create a Car class
 * that inherits from Vehicle. Then create another class that 
 * inherits from the car class. 
 * You should be able to handle steering, changing gears, and 
 * moving (speed and rotation), increase and decrease speed. 
 * You need to decide where to put the methods and behaviours
 * For your specific type of car add something specific
 */
package com.mycompany.gd2vehicleinheritance;

/**
 *
 * @author administrator
 */
public class Vehicle {
    private String name;
    private String size;
    private double maxSpeed;
    
    private double currentSpeed;
    private double currentDirection;
    
    public Vehicle()
    {
        this.name = "NoName";
        this.size = "NoSize";
        this.maxSpeed = 0.0;
        
        this.currentSpeed = 0.0;
        this.currentDirection = 0.0;
    }

    public Vehicle(String name, String size, double maxSpeed) {
        this.name = name;
        this.size = size;
        this.maxSpeed = maxSpeed;
        
        this.currentSpeed = 0.0;
        this.currentDirection = 0.0;
    }
    
    
    
    public void steer(double direction)
    {
        this.currentDirection += direction;
        System.out.println("Vehicle.steer(): Steering at " + 
                currentDirection + " degrees");
    }
    
    public void move(double speed, double direction)
    {
        currentSpeed = speed;
        currentDirection = direction;
        System.out.println("Vehicle.move(): Vehicle is moving at "
                + currentSpeed + " in direction " + currentDirection);
    }
    
    public void stop(){
        this.currentSpeed = 0;
    }

    public String getName() {
        return name;
    }

    public String getSize() {
        return size;
    }

    public double getCurrentSpeed() {
        return currentSpeed;
    }

    public double getCurrentDirection() {
        return currentDirection;
    }
    
    public double getMaxSpeed() {
        return maxSpeed;
    }
    
}
