/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gd2vehicleinheritance;

/**
 *
 * @author administrator
 */
public class ShelbyGT500 extends Car{

    public ShelbyGT500() {
        super("ChevyGT500", "Coupe", 202.0, '6', 5, 2, false);
    }
    
    public void accelerate(double rate)
    {
        double newSpeed = getCurrentSpeed() + rate;
        double maxSpeed = getMaxSpeed();
        if(newSpeed > maxSpeed)
        {
            newSpeed = maxSpeed;
        }
        if(newSpeed == 0){
            stop();
            changeGear('N');
        }
        else if(newSpeed > 0 && newSpeed <= 40){
            changeGear('1');
        }
        else if(newSpeed > 40 && newSpeed <= 70){
            changeGear('2');
        }
        else if(newSpeed > 70 && newSpeed <= 100){
            changeGear('3');
        }
        else if(newSpeed > 100 && newSpeed <= 130){
            changeGear('4');
        }
        else if(newSpeed > 130 && newSpeed <= 160){
            changeGear('5');
        }
        else if(newSpeed > 160){
            changeGear('6');
        }
        
        if(newSpeed > 0){
            changeVelocity(newSpeed, getCurrentDirection());
        }
    }

    
    
    
    
}
