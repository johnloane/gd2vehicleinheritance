/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gd2vehicleinheritance;

/**
 *
 * @author administrator
 */
public class Car extends Vehicle{

    private int gears;
    private int wheels;
    private int doors;
    private boolean isManual;
    
    private char currentGear;
    
    public Car()
    {
        this("NoName", "NoSize", 0.0,  0, 0, 0, false);
    }
    
    public Car(String name, String size, double maxSpeed, int gears, 
            int wheels, int doors, boolean isManual) {
        super(name, size, maxSpeed);
        this.gears = gears;
        this.wheels = wheels;
        this.doors = doors;
        this.isManual = isManual;
        this.currentGear = 'N';
    }
    
    public void changeGear(char currentGear)
    {
        this.currentGear = currentGear;
        System.out.println("Car.setCurrentGear(): Changed to " 
                + this.currentGear + " gear");
    }

    public char getCurrentGear() {
        return currentGear;
    }
    
    public void changeVelocity(double speed, double direction)
    {
        System.out.println("Car.changeVelocity() : Velocity " + speed + 
                " direction " + direction);
        move(speed, direction);
    }
    

    
    
    
    
}
