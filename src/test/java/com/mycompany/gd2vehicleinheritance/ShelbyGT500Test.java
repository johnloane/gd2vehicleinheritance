/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gd2vehicleinheritance;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author administrator
 */
public class ShelbyGT500Test {
    
    public ShelbyGT500Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of accelerate method, of class ChevyGT500.
     */
    @Test
    public void testAccelerate() {
        System.out.println("accelerate");
        ShelbyGT500 instance = new ShelbyGT500();
        accelerateCheckSpeedAndGear(instance, 0, 0.0, 'N');
        accelerateCheckSpeedAndGear(instance, 1, 1.0, '1');
        accelerateCheckSpeedAndGear(instance, 39, 40.0, '1');
        accelerateCheckSpeedAndGear(instance, 1, 41.0, '2');
        accelerateCheckSpeedAndGear(instance, 29, 70.0, '2');
        accelerateCheckSpeedAndGear(instance, 1, 71.0, '3');
        accelerateCheckSpeedAndGear(instance, 29, 100.0, '3');
        accelerateCheckSpeedAndGear(instance, 1, 101.0, '4');
        accelerateCheckSpeedAndGear(instance, 29, 130.0, '4');
        accelerateCheckSpeedAndGear(instance, 1, 131.0, '5');
        accelerateCheckSpeedAndGear(instance, 29, 160.0, '5');
        accelerateCheckSpeedAndGear(instance, 1, 161.0, '6');
        accelerateCheckSpeedAndGear(instance, 41, 202.0, '6');
        accelerateCheckSpeedAndGear(instance, 41, 202.0, '6');
        accelerateCheckSpeedAndGear(instance, -202, 0.0, 'N');
        
        
    }
    
    public void accelerateCheckSpeedAndGear(ShelbyGT500 instance, double 
            rate, double expectedSpeed, char expectedGear)
    {
        instance.accelerate(rate);
        double currentSpeed = instance.getCurrentSpeed();
        assertEquals(expectedSpeed, currentSpeed, 0.0);
        char gear = instance.getCurrentGear();
        assertEquals(expectedGear, gear);
    }
    
}
