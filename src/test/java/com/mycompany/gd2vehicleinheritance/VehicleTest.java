/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gd2vehicleinheritance;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author administrator
 */
public class VehicleTest {
    
    public VehicleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of steer method, of class Vehicle.
     */
    @Test
    public void testSteer() {
        System.out.println("steer");
        double direction = 90.0;
        Vehicle instance = new Vehicle();
        testSteer(instance, direction, 90.0);
        testSteer(instance, direction, 180.0);
        direction = -45.0;
        testSteer(instance, direction, 135.0);
    }
    
    public void testSteer(Vehicle instance, double direction, double expected)
    {
        instance.steer(direction);
        double actualDirection = instance.getCurrentDirection();
        assertEquals(expected, actualDirection, 0.0); 
    }

    /**
     * Test of move method, of class Vehicle.
     */
    @Test
    public void testMove() {
        System.out.println("move");
        double speed = 10.0;
        double direction = 10.0;
        Vehicle instance = new Vehicle();
        instance.move(speed, direction);
        assertEquals(speed, instance.getCurrentSpeed(), 0.0);
        assertEquals(direction, instance.getCurrentDirection(), 0.0);
    }
    
    /**
     * Test of stop method, of class Vehicle.
     */
    @Test
    public void testStop() {
        System.out.println("stop");
        double speed = 10.0;
        double direction = 10.0;
        Vehicle instance = new Vehicle();
        instance.move(speed, direction);
        instance.stop();
        speed = 0.0;
        assertEquals(speed, instance.getCurrentSpeed(), 0.0);
    }

    /**
     * Test of getName method, of class Vehicle.
     */
    @Ignore
    public void testGetName() {
        System.out.println("getName");
        Vehicle instance = new Vehicle();
        String expResult = "";
        String result = instance.getName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSize method, of class Vehicle.
     */
    @Ignore
    public void testGetSize() {
        System.out.println("getSize");
        Vehicle instance = new Vehicle();
        String expResult = "";
        String result = instance.getSize();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCurrentSpeed method, of class Vehicle.
     */
    @Ignore
    public void testGetCurrentSpeed() {
        System.out.println("getCurrentSpeed");
        Vehicle instance = new Vehicle();
        double expResult = 0.0;
        double result = instance.getCurrentSpeed();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCurrentDirection method, of class Vehicle.
     */
    @Ignore
    public void testGetCurrentDirection() {
        System.out.println("getCurrentDirection");
        Vehicle instance = new Vehicle();
        double expResult = 0.0;
        double result = instance.getCurrentDirection();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
